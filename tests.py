# -*- coding:utf-8 -*-
'''
Created on 2014. 5. 23.

@author: cinos81
'''

import unittest
from WeliboTranslator import WeliboTranslator

class weliboPostTest(unittest.TestCase):    
    def test_FromJapanase_ToKorean(self):
        param = {'originalText':'日本語'}
        translatedData =  WeliboTranslator(param).getData()
        self.assertEqual(200, translatedData.get('response').getcode())
        self.assertEqual("일본어", translatedData.get('text'))
            
if __name__ == "__main__":
    unittest.main()
