#-*- coding: utf-8 -*-
'''
Created on 2014. 5. 23.

@author: cinos81
'''

import urllib2
from httplib2.socks import HTTPError
from urllib2 import URLError
from bs4 import BeautifulSoup 
import urllib

class WeliboTranslator(object):
    defaultParam = {
     'lp':'JK',
     'originalText':'日本語を韓国語に翻訳して見ましょう',
     'sentenceStyle':'written',
     'url':'http://translate.weblio.jp/korean/'
    }
    translatedData = {'response':{},'text':''}
    
    def __init__(self, inputtedParam):
        self.param = dict(self.defaultParam.items() + inputtedParam.items())
        
        try:
            url = self.param.get('url')
            param = urllib.urlencode(self.param)
            self.res  = urllib2.urlopen(url, param)
            if self.res.getcode() == 200:
                soup = BeautifulSoup(self.res)
                self.translatedData['text'] = soup.find("input", {"id": "reTranslatedText"})['value']
        except HTTPError as e:
            pass
        except URLError as e:
            pass
        finally:
            self.translatedData['response'] = self.res
            
    def getData(self):
        return self.translatedData
        
if __name__ == '__main__':
    param = {'originalText':'品薄ところか黄色一色だ'}
    print WeliboTranslator(param).getData()['text']